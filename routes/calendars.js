const express = require('express');
const router = express.Router();
const request = require('request');

router.get('/', async function (req, res) {
    request.get("http://localhost:8080/api/calendars", (err, response, body) => {
        const calendars = JSON.parse(body);
        res.render('calendars', {calendars, title: 'Calendars'})
    })
});

router.get('/:calendarId', async function (req, res) {
    const {calendarId} = req.params;
    request.get(`http://localhost:8080/api/calendars/${calendarId}`, (err, response, body) => {
        const calendar = JSON.parse(body);
        console.log(calendar)
        res.render('calendar', {calendar, title: calendar.name})
    })
})

module.exports = router;
