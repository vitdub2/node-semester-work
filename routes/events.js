const express = require('express');
const router = express.Router();
const request = require('request');

router.get('/', async function (req, res) {
    request.get("http://localhost:8080/api/events", (err, response, body) => {
        const events = JSON.parse(body);
        res.render('events', {events, title: 'Events'})
    })
})

router.get('/:eventId', async function (req, res) {
    const {eventId} = req.params;
    request.get(`http://localhost:8080/api/events/${eventId}`, (err, response, body) => {
        const event = JSON.parse(body);
        res.render('event', {event, title: event.info.name})
    })
});

module.exports = router;
